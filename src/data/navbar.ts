import type { Link } from '../types/link'

export const navbarLinks: Link[] = [
    {
        label: 'Home',
        url: '/',
    },
    {
        label: 'Galleries',
        url: '/galleries',
    },
    {
        label: 'CMD Gen',
        url: '/commandGenerators',
    },
]
