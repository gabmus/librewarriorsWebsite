import type { CommandBuilder } from '../../types/commandBuilderParam'

const description = `
- \`compatibilityLevel\`:
  - \`1.1\`
  - \`1.2\`
  - \`1.3\`
  - \`1.4\`
`

export const compressPdf: CommandBuilder = {
    title: 'Compress PDF with Ghostscript',
    description,
    inputData: {
        fileName: {
            type: 'string',
        },
        compatibilityLevel: {
            type: 'string',
        },
        outputFile: {
            type: 'string',
        },
    },
    build: function (data) {
        let compatLevel = data.compatibilityLevel.value
            ? (data.compatibilityLevel.value as string).trim()
            : '1.4'
        if (!['1.1', '1.2', '1.3', '1.4'].includes(compatLevel)) {
            compatLevel = '1.4'
        }

        const outFile = data.outputFile.value
            ? (data.outputFile.value as string).trim().replaceAll("'", "\\'")
            : 'out.pdf'

        const inFile = data.fileName.value
            ? (data.fileName.value as string).trim().replaceAll("'", "\\'")
            : 'inputFile.pdf'

        return [
            'gs',
            '-q -dNOPAUSE -dBATCH -dSAFER',
            '-sDEVICE=pdfwrite',
            `-dCompatibilityLevel=${compatLevel}`,
            '-dPDFSETTINGS=/screen',
            '-dEmbedAllFonts=true -dSubsetFonts=true',
            '-dCompressFonts=true',
            '-r150',
            '-dDetectDuplicateImages',
            `-sOutputFile='${outFile}'`,
            `'${inFile}'`,
        ].join(' ')
    },
}
