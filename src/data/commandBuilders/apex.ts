import type { CommandBuilder } from '../../types/commandBuilderParam'

export const apex: CommandBuilder = {
    title: 'Apex Legends launch options',
    inputData: {
        fsr: {
            type: 'boolean',
        },
        dx12: {
            type: 'boolean',
        },
        mangoHud: {
            type: 'boolean',
        },
        noIntroVideo: {
            type: 'boolean',
        },
    },
    build: function (data) {
        const parts = [
            data.mangoHud.value ? 'MANGOHUD=1' : undefined,
            data.fsr.value ? 'gamescope -U -f --' : undefined,
            '%command%',
            data.dx12.value
                ? '-eac_launcher_settings SettingsDX12.json'
                : undefined,
            data.noIntroVideo.value ? '-novid' : undefined,
        ]
        return parts.filter((part) => !!part).join(' ')
    },
}
