import { apex } from './apex'
import { compressPdf } from './compressPdf'

export const builders = {
    apex,
    compressPdf,
}

export type BuilderKey = keyof typeof builders
