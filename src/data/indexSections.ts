import type { SectionData } from '../types/sectionData'

export const indexSections: SectionData[] = [
    {
        title: 'Games',
        description: 'The games we play on a regular basis',
        cards: [
            {
                title: 'Apex Legends',
                description: 'Regular all day shooting',
                links: [
                    {
                        label: 'Steam',
                        url: 'https://store.steampowered.com/app/1172470/apex_legends',
                    },
                ],
                image: '/cards/apexlegends.jpg',
            },
            {
                title: 'Valheim',
                description: 'Occasional base building\n\n**Epic** adventuring',
                links: [
                    {
                        label: 'Steam',
                        url: 'https://store.steampowered.com/app/892970/Valheim',
                    },
                ],
                image: '/cards/valheim.jpg',
            },
        ],
    },
    {
        title: 'Apps',
        description: 'Apps we like and use',
        cards: [
            {
                title: 'Element',
                description: 'Private, decentralized and federated chat',
                links: [
                    {
                        label: 'Website',
                        url: 'https://element.io',
                    },
                ],
                image: '/cards/matrix.svg',
                icon: '/icons/element.svg',
            },
            {
                title: 'Jitsi Meet',
                description: 'In browser VoIP',
                links: [
                    {
                        label: 'Website',
                        url: 'https://meet.jit.si',
                    },
                ],
            },
            {
                title: 'Heroic Games Launcher',
                links: [
                    {
                        label: 'Website',
                        url: 'https://heroicgameslauncher.com/',
                    },
                    {
                        label: 'Flathub',
                        url: 'https://flathub.org/apps/com.heroicgameslauncher.hgl',
                    },
                ],
            },
            {
                title: 'Bottles',
                links: [
                    {
                        label: 'Website',
                        url: 'https://usebottles.com',
                    },
                ],
            },
        ],
    },
    {
        title: 'Websites',
        description: 'Useful or just plain cool websites',
        cards: [
            {
                title: 'Flathub',
                links: [
                    {
                        label: 'Website',
                        url: 'https://flathub.org',
                    },
                ],
            },
            {
                title: 'ProtonDB',
                description: 'Steam games compatibility with Proton',
                links: [
                    {
                        label: 'Website',
                        url: 'https://protondb.com',
                    },
                ],
            },
            {
                title: 'Are We Anti-Cheat Yet?',
                description:
                    'A crowd-sourced list of games using anti-cheats and their compatibility with GNU/Linux or Wine/Proton',
                links: [
                    {
                        label: 'Website',
                        url: 'https://areweanticheatyet.com/',
                    },
                ],
            },
            {
                title: 'Gaming on Linux',
                description:
                    'Linux Gaming, SteamOS and Steam Deck gaming. Covering Linux Gaming News, Linux Games, SteamOS, Indie Game Reviews, Steam Play Proton and more',
                links: [
                    {
                        label: 'Website',
                        url: 'https://gamingonlinux.com',
                    },
                ],
            },
        ],
    },
    {
        title: 'Distros',
        description: 'Best distros for gaming and every day use',
        cards: [
            {
                title: 'Fedora',
                description:
                    'Go-to mainstream distro; good defaults, up to date and easy to use',
                links: [
                    {
                        label: 'Website',
                        url: 'https://fedoraproject.org/',
                    },
                ],
            },
            {
                title: 'Nobara',
                description:
                    'Fedora based distros with tweaks, improvements and better defaults for general desktop and gaming use',
                links: [
                    {
                        label: 'Website',
                        url: 'https://nobaraproject.org/',
                    },
                ],
            },
            {
                title: 'Arch',
                description:
                    'Build your distro exactly the way you want it; only for experienced users',
                links: [
                    {
                        label: 'Website',
                        url: 'https://archlinux.org/',
                    },
                ],
            },
        ],
    },
]
