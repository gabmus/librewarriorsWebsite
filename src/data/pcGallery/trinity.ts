import type { GalleryData } from '../../types/galleryData'

const description = `
The OG, continuously upgraded throughout the years

Specs:

- Ryzen 7 5800X
  - Cooled by the Noctua NH-D15 (_brown fans are a status symbol_)
- 32GB DDR4@3600MT/s
- Radeon RX 5700XT

Running Arch Linux BTW
`

export const trinity: GalleryData = {
    title: 'Trinity-Zero',
    description,
    author: 'GabMus',
    pictures: [
        {
            url: '/galleries/pc/trinity/trinity_0.jpg',
        },
    ],
}
