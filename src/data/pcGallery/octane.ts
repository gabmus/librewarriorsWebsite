import type { GalleryData } from '../../types/galleryData'

const description = `
Octane

Specs:

- Ryzen 5 5600
  - Cooled by the Scythe Fuma 2
- 32GB DDR4@3200MT/s
- Radeon RX 6700
`

export const octane: GalleryData = {
    title: 'Octane',
    description,
    author: 'Maccus',
    pictures: [
        {
            url: '/galleries/pc/octane/octanePc.jpg',
        },
    ],
}
