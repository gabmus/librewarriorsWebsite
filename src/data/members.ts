import type { Member } from '../types/member'

export const members: Member[] = [
    {
        name: 'GabMus',
        avatar: '/icons/gabmus.svg',
        links: [
            {
                label: 'Website',
                url: 'https://gabmus.org',
                icon: '/icons/globe.svg',
            },
            {
                label: 'GitLab',
                url: 'https://gitlab.com/gabmus',
                icon: '/icons/gitlab.svg',
            },
            {
                label: 'Mastodon',
                url: 'https://fosstodon.org/@gabmus',
                icon: '/icons/mastodon.svg',
            },
            {
                label: 'Steam',
                url: 'https://steamcommunity.com/id/gabmus/',
                icon: '/icons/steam.svg',
            },
        ],
    },
    {
        name: 'Maccus',
        avatar: '/icons/octane.png',
        links: [
            {
                label: 'GitLab',
                url: 'https://gitlab.com/MuscoMarco',
                icon: '/icons/gitlab.svg',
            },
            {
                label: 'Steam',
                url: 'https://steamcommunity.com/profiles/76561198147906939/',
                icon: '/icons/steam.svg',
            },
        ],
    },
    {
        name: 'Magbano',
        avatar: '/icons/magellano.jpg',
        links: [
            {
                label: 'GitLab',
                url: 'https://gitlab.com/davide.musco98',
                icon: '/icons/gitlab.svg',
            },
            {
                label: 'Steam',
                url: 'https://steamcommunity.com/id/magellano/',
                icon: '/icons/steam.svg',
            },
        ],
    },
]
