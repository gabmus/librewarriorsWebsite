import { createHash, randomUUID } from 'crypto'

export function genUID(obj: any): string {
    const hash = createHash('sha256')
    hash.update(JSON.stringify(obj))
    const uid = randomUUID() + hash.digest('hex')
    return uid
}
