export function normalizeId(text: string): string {
    return text.toLowerCase().replaceAll(' ', '-')
}
