import type { Link } from '../types/link'

export type Member = {
    name: string
    avatar?: string
    description?: string
    links?: Link[]
}
