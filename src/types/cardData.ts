import type { Link } from './link'

export type CardData = {
    title: string
    description?: string
    links?: Link[]
    image?: string
    icon?: string
}
