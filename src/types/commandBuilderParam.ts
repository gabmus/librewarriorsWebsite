export type CommandData = {
    type: 'string' | 'number' | 'boolean'
    value?: string | number | boolean
}

export type CommandBuilder = {
    title: string
    description?: string
    inputData: Record<string, CommandData>
    build: (data: Record<string, CommandData>) => string
}
