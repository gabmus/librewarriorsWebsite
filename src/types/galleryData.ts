export type GalleryData = {
    title: string
    author: string
    description?: string
    pictures: {
        url: string
        caption?: string
    }[]
}
