import type { CardData } from './cardData'

export type SectionData = {
    title: string
    description: string
    cards: CardData[]
}
